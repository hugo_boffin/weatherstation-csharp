﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace SerialLogger
{
    internal class SerialLogger
    {
        private readonly SerialPort _port = new SerialPort("COM3", 9600, Parity.None, 8, StopBits.One);
        private readonly Regex _regex = new Regex(@">>DATA,(?<hum>\d+),(?<tmp>\d+)<<", RegexOptions.Compiled);

        public void Start()
        {
            _port.DataReceived += OnPortOnDataReceived;

            _port.Open();
        }

        private void OnPortOnDataReceived(object sender, SerialDataReceivedEventArgs eventArgs)
        {
            try
            {
                var line = _port.ReadLine();
                var match = _regex.Match(line);
                if (match.Success)
                {
                    var now = DateTime.Now;
                    var hum = int.Parse(match.Groups["hum"].Value);
                    var tmp = int.Parse(match.Groups["tmp"].Value);

                    var settings = ConfigurationManager.ConnectionStrings;

                    using (var connection = new SqlConnection(settings[0].ConnectionString))
                    {
                        var cmd = new SqlCommand
                        {
                            CommandText = string.Format("INSERT INTO dbo.SensorData (eventdate, eventtime, humidity, temperature) VALUES('{0}','{1}',{2},{3})", now.ToString("yyyy-MM-dd"), now.ToString("HH:mm:ss"), hum, tmp), CommandType = CommandType.Text, Connection = connection
                        };

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
//                        LoggerService.eventLog.WriteEntry("Falsche Formatierung: '" + line + "'");
                }
            }
            catch (Exception e)
            {
//                    LoggerService.eventLog.WriteEntry("Fehler: " + e.ToString());
            }
        }

        public void Stop()
        {
            _port.Close();
        }
    }
}