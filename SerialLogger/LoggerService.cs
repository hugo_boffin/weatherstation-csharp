﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace SerialLogger
{
    public partial class LoggerService : ServiceBase
    {
        private SerialLogger _logger;
//        public static EventLog eventLog;

        public LoggerService()
        {
            InitializeComponent();
//            this.AutoLog = false;
//
//            eventLog = new System.Diagnostics.EventLog();
//            if (!System.Diagnostics.EventLog.SourceExists("SerialLogger"))
//            {
//                System.Diagnostics.EventLog.CreateEventSource("SerialLogger", "MyNewLog");
//            }
//            eventLog.Source = "SerialLogger";
//            eventLog.Log = "MyNewLog";
        }

        protected override void OnStart(string[] args)
        {
//            eventLog.WriteEntry("Start");
            _logger = new SerialLogger();
            _logger.Start();
        }

        protected override void OnStop()
        {
//            eventLog.WriteEntry("Stop");
            _logger.Stop();
        }

        internal void TestStartupAndStop(string[] args)
        {
            OnStart(args);
            Console.ReadLine();
            OnStop();
        }
    }
}