﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Prototypes
{
    class Program
    {
        private static readonly SerialPort Port = new SerialPort("COM3", 9600, Parity.None, 8, StopBits.One);

        [STAThread]
        static void Main(string[] args)
        {
//            string line = ">>DATA,13,26<<";

            var regex = new Regex(@">>DATA,(?<hum>\d+),(?<tmp>\d+)<<", RegexOptions.Compiled);
            
            Port.DataReceived += new SerialDataReceivedEventHandler((sender, eventArgs) =>
            {
                var match = regex.Match(Port.ReadLine());
                if (match.Success)
                {
                    var now = DateTime.Now;
                    var hum = int.Parse(match.Groups["hum"].Value);
                    var tmp = int.Parse(match.Groups["tmp"].Value);

                    Console.WriteLine(hum + " " + tmp);

                    ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
                    
                    using (var connection = new SqlConnection(settings[0].ConnectionString))
                    {
                        var cmd = new SqlCommand
                        {
                            CommandText =
                                string.Format(
                                    "INSERT INTO dbo.SensorData (eventdate, eventtime, humidity, temperature) VALUES('{0}','{1}',{2},{3})",
                                    now.ToString("yyyy-MM-dd"), now.ToString("HH:mm:ss"), hum, tmp),
                            CommandType = CommandType.Text,
                            Connection = connection
                        };

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            });

            Port.Open();
            Console.ReadKey();
        }
    }
}
